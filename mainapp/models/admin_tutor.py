# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

class AdminTutor(models.Model):
    name = models.TextField()
    email = models.EmailField(primary_key=True)
    password = models.TextField(default="")
    role = models.TextField()