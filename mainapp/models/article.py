# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from ..constants.string_constants import StringConstants

class Article(models.Model):
    article_code = models.TextField(primary_key=True)
    article_title = models.TextField()
    article_content =  models.TextField()# Stored as a JSON string
    author_email = models.EmailField()
    article_status = models.TextField(default=StringConstants.UNPUBLISHED)
