# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from bson.json_util import default
from ..constants.string_constants import StringConstants

class Course(models.Model):
    course_name = models.TextField()
    course_code = models.TextField(primary_key=True)
    course_tutor_email = models.EmailField(default="")
    course_category = models.TextField(default="")
    course_status = models.TextField(default=StringConstants.UNPUBLISHED)
    
class Section(models.Model):
    course_code = models.TextField()
    section_title = models.TextField()
    section_content =  models.TextField()# Stored as a JSON string
    section_pos = models.AutoField(primary_key=True)
    section_status = models.TextField(default=StringConstants.UNPUBLISHED)