# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from ..serializers import ArticleSerializer
from ..models import Article
from rest_framework.response import Response
from rest_framework import status
from rest_framework.decorators import api_view
from ..constants.string_constants import StringConstants
from ..util.aws import remove_deleted_images, remove_all_deleted_images

##
# Get article
#
@api_view(['POST'])
def get_article(request, format=None):
    a_code = request.data.get('article_code')
    try:
        article = Article.objects.get(article_code=a_code)
        serializer = ArticleSerializer(article, many=False)
        response = serializer.data
    except Article.DoesNotExist:
        response = {}
    return Response(response)

##
# Get all articles
#
@api_view(['GET'])
def get_all_articles(request, format=None):
    articles = Article.objects.all()
    serializer = ArticleSerializer(articles, many=True)
    return Response(serializer.data)

##
# Get all published articles
#
@api_view(['GET'])
def get_all_published_articles(request, format=None):
    articles = Article.objects.filter(article_status=StringConstants.PUBLISHED)
    serializer = ArticleSerializer(articles, many=True)
    return Response(serializer.data)

##
# Get user articles
#
@api_view(['POST'])
def get_user_articles(request, format=None):
    a_email = request.data.get('author_email')
    try:
        articles = Article.objects.filter(author_email=a_email)
        serializer = ArticleSerializer(articles, many=True)
        response = serializer.data
    except Article.DoesNotExist:
        response = []
    return Response(response)

##
# Create/update article
#
@api_view(['POST'])
def create_update_article(request, update=''):
    article_code=request.data['article_code']
    if update.strip() == 'update':
        article = Article.objects.get(article_code=request.data['article_code'])
        remove_deleted_images(article.article_content, request.data['article_content'])
        serializer = ArticleSerializer(article, data=request.data)
    else:
        serializer = ArticleSerializer(data=request.data, many=False)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data, status=status.HTTP_200_OK)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

##
# Remove article
#
@api_view(['POST'])
def remove_article(request, format=None):
    a_code = request.data.get('article_code')
    try:
        article = Article.objects.get(article_code=a_code)
        remove_all_deleted_images(article.article_content)
        article.delete()
    except Article.DoesNotExist:
        return Response({}, status=status.HTTP_400_BAD_REQUEST)
    return Response({}, status=status.HTTP_200_OK)
