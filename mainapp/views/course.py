# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from ..serializers import CourseSerializer, SectionSerializer
from ..models import Course, Section
from rest_framework.response import Response
from rest_framework import status
from rest_framework.decorators import api_view
import json
import os
import boto
from boto.s3.key import Key
from django.conf import settings
import uuid
import re
from operator import itemgetter
from ..constants.string_constants import StringConstants
from ..util.aws import remove_deleted_images, remove_all_deleted_images

##
# Get course info
#
@api_view(['POST'])
def get_course(request, format=None):
    c_code = request.data.get('course_code')
    try:
        course = Course.objects.get(course_code=c_code)
        serializer = CourseSerializer(course, many=False)
        response = serializer.data
    except Course.DoesNotExist:
        response = {}
    return Response(response)

##
# Get all course info
#
@api_view(['GET'])
def get_all_courses(request, format=None):
    courses = Course.objects.all()
    serializer = CourseSerializer(courses, many=True)
#     print serializer.data
    for course_pack in serializer.data:
        course = dict(course_pack)
        c_code = course['course_code']
        try:
            sections = Section.objects.filter(course_code=c_code)
            serializer2 = SectionSerializer(sections, many=True)
            course_pack['sections_length'] = len(sections)
            num_published = 0;
            for sec_pack in serializer2.data:
                sec = dict(sec_pack)
                if (sec['section_status'] == StringConstants.PUBLISHED):
                    num_published += 1
            course_pack['num_published_sections'] = num_published
        except Section.DoesNotExist:
            course_pack['sections_length']= 0
    return Response(serializer.data)

##
# Get all published course info
#
@api_view(['GET'])
def get_all_published_courses(request, format=None):
    courses = Course.objects.filter(course_status=StringConstants.PUBLISHED)
    serializer = CourseSerializer(courses, many=True)
#     print serializer.data
    for course_pack in serializer.data:
        course = dict(course_pack)
        c_code = course['course_code']
        try:
            sections1 = Section.objects.filter(course_code=c_code)
            serializer2 = SectionSerializer(sections1, many=True)
            course_pack['sections_length'] = len(sections1)
        except Section.DoesNotExist:
            course_pack['sections_length']= 0
        try:
            sections2 = Section.objects.filter(course_code=c_code, section_status=StringConstants.PUBLISHED).order_by('section_pos')
            serializer3 = SectionSerializer(sections2, many=True)
            course_pack['num_published_sections'] = len(serializer3.data)
            if len(serializer3.data) > 0:
                sect_data = dict(serializer3.data[0])
                course_pack['first_section_title'] = sect_data['section_title']
        except Section.DoesNotExist:
            course_pack['num_published_sections']= 0
            course_pack['first_section_title'] = ""
    return Response(serializer.data)

##
# Get a published course section list
#
@api_view(['POST'])
def get_published_course_section_list(request, format=None):
    c_code = request.data.get('course_code')
    try:
        course = Course.objects.filter(course_code=c_code, course_status=StringConstants.PUBLISHED)
        sections = Section.objects.filter(course_code=c_code, section_status=StringConstants.PUBLISHED).order_by('section_pos')
        serializer = SectionSerializer(sections, many=True)
        response = serializer.data
    except Course.DoesNotExist, Section.DoesNotExist:
        response = []
    return Response(response)

##
# Get a published course section
#
@api_view(['POST'])
def get_published_course_section(request, format=None):
    c_code = request.data.get('course_code')
    s_title = request.data.get('section_title')
    try:
        course = Course.objects.filter(course_code=c_code, course_status=StringConstants.PUBLISHED)
        section = Section.objects.filter(course_code=c_code,
                                         section_title=s_title,
                                         section_status=StringConstants.PUBLISHED)
        serializer = SectionSerializer(section, many=True)
        if len(serializer.data) > 0:
            response = serializer.data[0]
        else:
            response = {}
    except Course.DoesNotExist, Section.DoesNotExist:
        response = {}
    return Response(response)

##
# Get a published course section
#
@api_view(['POST'])
def get_published_course_first_section(request, format=None):
    c_code = request.data.get('course_code')
    try:
        course = Course.objects.filter(course_code=c_code, course_status=StringConstants.PUBLISHED)
        sections = Section.objects.filter(course_code=c_code, section_status=StringConstants.PUBLISHED)
        serializer = SectionSerializer(sections, many=True)
        if len(serializer.data) > 0:
            response = serializer.data[0]
        else:
            response = {}
    except Course.DoesNotExist, Section.DoesNotExist:
        response = {}
    return Response(response)


##
# Get user course info
#
@api_view(['POST'])
def get_user_courses(request, format=None):
    t_email = request.data.get('course_tutor_email')
    try:
        courses = Course.objects.filter(course_tutor_email=t_email)
        serializer = CourseSerializer(courses, many=True)
        for course_pack in serializer.data:
            course = dict(course_pack)
            c_code = course['course_code']
            try:
                sections = Section.objects.filter(course_code=c_code)
                serializer2 = SectionSerializer(sections, many=True)
                course_pack['sections_length'] = len(sections)
                num_published = 0;
                for sec_pack in serializer2.data:
                    sec = dict(sec_pack)
                    if (sec['section_status'] == StringConstants.PUBLISHED):
                        num_published += 1
                course_pack['num_published_sections'] = num_published
            except Section.DoesNotExist:
                course_pack['sections_length']= 0
        response = serializer.data
    except Course.DoesNotExist:
        response = []
    return Response(response)

##
# Create/update course
#
@api_view(['POST'])
def create_update_course(request, update=''):
    c_code=request.data['course_code']
    if update.strip() == 'update':
        course = Course.objects.get(course_code=request.data['course_code'])
        serializer = CourseSerializer(course, data=request.data)
    else:
        serializer = CourseSerializer(data=request.data, many=False)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data, status=status.HTTP_200_OK)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

##
# Remove course
#
@api_view(['POST'])
def remove_course(request, format=None):
    c_code = request.data.get('course_code')
    try:
        course = Course.objects.get(course_code=c_code)
        course.delete()
    except Course.DoesNotExist:
        return Response({}, status=status.HTTP_400_BAD_REQUEST)
    return Response({}, status=status.HTTP_200_OK)

##
# Get course sections
#
@api_view(['POST'])
def get_course_sections(request, format=None):
    c_code = request.data.get('course_code')
    try:
        sections = Section.objects.filter(course_code=c_code)
        serializer = SectionSerializer(sections, many=True)
        response = serializer.data
    except Section.DoesNotExist:
        response = []
    return Response(response)

##
# Create/update new course section
#
@api_view(['POST'])
def create_update_course_section(request, section_pos=-1):
    if section_pos != -1: #update
        section = Section.objects.get(section_pos=section_pos)
        remove_deleted_images(section.section_content, request.data['section_content'])
        serializer = SectionSerializer(section, data=request.data)
    else:
        serializer = SectionSerializer(data=request.data, many=False)
    if serializer.is_valid():
        serializer.save()
        try:
            c_code = request.data['course_code']
            sections = Section.objects.filter(course_code=c_code)
            serializer2 = SectionSerializer(sections, many=True)
            num_published = 0
            for sec_pack in serializer2.data:
                sec = dict(sec_pack)
                if (sec['section_status'] == StringConstants.PUBLISHED):
                    num_published += 1
            course = Course.objects.get(course_code=c_code)
            if num_published == 0:
                course.course_status = StringConstants.UNPUBLISHED
                course.save()
        except Section.DoesNotExist, Course.DoesNotExist:
            return Response(serializer3.errors, status=status.HTTP_400_BAD_REQUEST)
        return Response(serializer.data, status=status.HTTP_200_OK)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

##
# Upload image
#
@api_view(['POST'])
def upload_image(request):
    response = ''
    try:
        config = json.loads(open(os.path.join(settings.BASE_DIR,'mainapp/config/credentials.json')).read())
        conn = boto.connect_s3(config['aws_access_key_id_s3'],config["aws_secret_access_key_s3"]) #Connect to S3
        bucket = conn.get_bucket("boesk-image-uploads")
        k = Key(bucket)
        ext = os.path.splitext(request.data['image'].name)[1]
        k.key = 'tutor-boesk/' + str(uuid.uuid4()) + str(ext)
        k.set_contents_from_string(request.data['image'].read(), replace=True)
        response = {
            "data":{
                "type": request.data['image'].content_type,
                "size": request.data['image'].size,
                "link": os.path.join(config['tutor-boesk-image-url-base'], k.key)
            },
            "success": True,
            "status": status.HTTP_200_OK
        }
    except Exception as e:
        return Response({}, status=status.HTTP_400_BAD_REQUEST)

    return Response(response)

##
# Remove course
#
@api_view(['POST'])
def remove_course_section(request, format=None):
    c_code = request.data.get('course_code')
    section_pos = request.data.get('section_pos')
    try:
        section = Section.objects.get(course_code=c_code, section_pos=section_pos)
        remove_all_deleted_images(section.section_content)
        section.delete()
    except Section.DoesNotExist:
        return Response({}, status=status.HTTP_400_BAD_REQUEST)
    return Response({}, status=status.HTTP_200_OK)
