# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from rest_framework.response import Response
from rest_framework import status
from rest_framework.decorators import api_view
from ..models import AdminTutor
from ..serializers import AdminTutorSerializer
from django.template.context_processors import request
from django.contrib.auth.hashers import make_password, check_password
import collections

##
# Get all admins
#
@api_view(['POST'])
def authenticate(request, format=None):
    email = request.data.get('email')
    password = request.data.get('password')
    response = {}
    try:
        user = AdminTutor.objects.get(email=email)
        serializer = AdminTutorSerializer(user, many=False)
        if (check_password(password, serializer.data['password'])):
            response = serializer.data
    except AdminTutor.DoesNotExist:
        pass
    return Response(response)

##
# Get all admins
#
@api_view(['GET'])
def get_all_users(request, format=None):
    try:
        users = AdminTutor.objects.all()
        serializer = AdminTutorSerializer(users, many=True)
        response = serializer.data
    except AdminTutor.DoesNotExist:
        response = []
    return Response(response)
   
##
# Get all admins
#
@api_view(['GET'])
def get_admins(request, format=None):
    try:
        admin = AdminTutor.objects.filter(role='Admin')
        serializer = AdminTutorSerializer(admin, many=True)
        response = serializer.data
    except AdminTutor.DoesNotExist:
        response = []
    return Response(response)
    
##
# Get all admins
#
@api_view(['GET'])
def get_tutors(request, format=None):
    try:
        tutor = AdminTutor.objects.filter(role='Tutor')
        serializer = AdminTutorSerializer(tutor, many=True)
        response = serializer.data
    except AdminTutor.DoesNotExist:
        response = []
    return Response(response)

##
# Create admin/tutor
#
@api_view(['POST'])
def create_update_admin_tutor(request, update=''):
    if update.strip() == 'update':
        admin_tutor = AdminTutor.objects.get(email=request.data['email'])
        request.data['password'] = make_password(request.data['password'])
        serializer = AdminTutorSerializer(admin_tutor, data=request.data)
    else:
        request.data['password'] = make_password(request.data['password'])
        serializer = AdminTutorSerializer(data=request.data, many=False)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data, status=status.HTTP_200_OK)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

##
# Remove admin/tutor
#
@api_view(['POST'])
def remove_admin_tutor(request):
    email = request.data.get('email')
    try:
        user = AdminTutor.objects.get(email=email)
        user.delete()
        return Response({}, status=status.HTTP_200_OK)
    except AdminTutor.DoesNotExist:
        return Response({}, status=status.HTTP_400_BAD_REQUEST)
