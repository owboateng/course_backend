class StringConstants:
    PUBLISHED = "Published"
    UNPUBLISHED = "Unpublished"
    ADMIN_NAME = "Main Admin"
    ADMIN_EMAIL = "admin@tutor.boesk.com"
    ADMIN_ROLE = "Admin"