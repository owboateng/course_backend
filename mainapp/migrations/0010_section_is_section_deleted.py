# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-07-20 20:38
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mainapp', '0009_course_course_status'),
    ]

    operations = [
        migrations.AddField(
            model_name='section',
            name='is_section_deleted',
            field=models.BooleanField(default=False),
        ),
    ]
