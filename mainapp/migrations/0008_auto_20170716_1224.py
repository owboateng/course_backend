# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-07-16 12:24
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mainapp', '0007_auto_20170716_1221'),
    ]

    operations = [
        migrations.AlterField(
            model_name='section',
            name='section_content',
            field=models.TextField(),
        ),
    ]
