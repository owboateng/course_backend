# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-07-04 19:26
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mainapp', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='course',
            name='course_tutor_email',
        ),
    ]
