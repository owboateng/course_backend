from django.conf import settings
import json
import os
import boto
import re
from boto.s3.key import Key

##
# Find and remove all deleted images from aws s3
#
def remove_deleted_images(prev_content, curr_content):
    config = json.loads(open(os.path.join(settings.BASE_DIR,'mainapp/config/credentials.json')).read())
    prev_urls = re.findall('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', prev_content)
    curr_urls = re.findall('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', curr_content)
    conn = boto.connect_s3(config['aws_access_key_id_s3'],config["aws_secret_access_key_s3"]) #Connect to S3
    bucket = conn.get_bucket("boesk-image-uploads")
    k = Key(bucket)
    for url in prev_urls:
        if (config['tutor-boesk-image-url-base'] in url) and url not in curr_urls:
            k.key = 'tutor-boesk/' + os.path.basename(url)
            k.delete()


##
# Remove all deleted images from aws s3
#
def remove_all_deleted_images(content):
    config = json.loads(open(os.path.join(settings.BASE_DIR,'mainapp/config/credentials.json')).read())
    urls = re.findall('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', content)
    conn = boto.connect_s3(config['aws_access_key_id_s3'],config["aws_secret_access_key_s3"]) #Connect to S3
    bucket = conn.get_bucket("boesk-image-uploads")
    k = Key(bucket)
    for url in urls:
        if config['tutor-boesk-image-url-base'] in url:
            k.key = 'tutor-boesk/' + os.path.basename(url)
            k.delete()
