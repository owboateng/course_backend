from django.conf.urls import url
from .views import course
from .views import admin_tutor
from .views import article

urlpatterns = [
    # Courses
    url(r'^all_courses/$', course.get_all_courses, name='get_all_courses'),
    url(r'^all_published_courses/$', course.get_all_published_courses, name='get_all_published_courses'),
    url(r'^published_course_section_list/$', course.get_published_course_section_list, name='get_published_course_section_list'),
    url(r'^published_course_section/$', course.get_published_course_section, name='get_published_course_section'),
    url(r'^published_course_first_section/$', course.get_published_course_first_section, name='get_published_course_first_section'),
    url(r'^create_update_course/(?P<update>update)/$', course.create_update_course, name='create_update_course (update)'),
    url(r'^create_update_course/$', course.create_update_course, name='create_update_course'),
    url(r'^remove_course/$', course.remove_course, name='remove_course'),
    url(r'^course/$', course.get_course, name='get_course'),
    url(r'^user_courses/$', course.get_user_courses, name='get_user_courses'),
    url(r'^course_sections/$', course.get_course_sections, name='get_course_sections'),
    url(r'^create_update_course_section/(?P<section_pos>\d+)/$', course.create_update_course_section, name='create_update_course_section'),
    url(r'^create_update_course_section/$', course.create_update_course_section, name='create_update_course_section'),
    url(r'^remove_course_section/$', course.remove_course_section, name='remove_course_section'),

    # Util
    url(r'^upload_image/$', course.upload_image, name='upload_image'),

    # Articles
    url(r'^article/$', article.get_article, name='get_article'),
    url(r'^all_articles/$', article.get_all_articles, name='get_all_articles'),
    url(r'^all_published_articles/$', article.get_all_published_articles, name='get_all_published_articles'),
    url(r'^user_articles/$', article.get_user_articles, name='get_user_articles'),
    url(r'^create_update_article/(?P<update>update)/$', article.create_update_article, name='create_update_article'),
    url(r'^create_update_article/$', article.create_update_article, name='create_update_article'),
    url(r'^remove_article/$', article.remove_article, name='remove_article'),

    # Admins and Tutors
    url(r'^authenticate/$', admin_tutor.authenticate, name='authenticate'),
    url(r'^all_users/$', admin_tutor.get_all_users, name='get_all_users'),
    url(r'^admins/$', admin_tutor.get_admins, name='get_admins'),
    url(r'^tutors/$', admin_tutor.get_tutors, name='get_tutors'),
    url(r'^create_update_admin_tutor/(?P<update>update)/$', admin_tutor.create_update_admin_tutor, name='create_update_admin_tutor'),
    url(r'^create_update_admin_tutor/$', admin_tutor.create_update_admin_tutor, name='create_update_admin_tutor'),
    url(r'^remove_admin_tutor/$', admin_tutor.remove_admin_tutor, name='remove_admin_tutor'),
]
