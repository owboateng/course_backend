from rest_framework import serializers
from ..models import Course, Section, AdminTutor, Article

class CourseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Course
        fields = '__all__'

class SectionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Section
        fields = '__all__'

class AdminTutorSerializer(serializers.ModelSerializer):
    class Meta:
        model = AdminTutor
        fields = '__all__'

class ArticleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Article
        fields = '__all__'
