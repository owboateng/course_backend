from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.hashers import make_password
from ...serializers import AdminTutorSerializer
from ...models import AdminTutor
from ...constants.string_constants import StringConstants
import re

class Command(BaseCommand):
    help = 'Updates admin info'
    
    def __validate_pass(self, pwd):
        if len(pwd) < 6:
            return ("Password must be at least 6 characters!")
        if not re.search(r'\d', pwd):
            return ("password must contain at least one number (0-9)!")
        if not re.search(r'[a-z]', pwd):
            return ("password must contain at least one lowercase letter (a-z)!")
        if not re.search(r'[A-Z]', pwd):
            return ("password must contain at least one uppercase letter (A-Z)!");
        return ''
    
    def add_arguments(self, parser):
        parser.add_argument(
            '--cp',
            help='Change default admin password',
        )
        
        parser.add_argument(
            '--add',
            action='store_true',
            dest='add',
            default=False,
            help='Add default admin'
        )
        
    def handle(self, *args, **options):
        if options['add']:
            try:
                admin_tutor = AdminTutor.objects.get(email=StringConstants.ADMIN_EMAIL)
            except AdminTutor.DoesNotExist:
                print "Adding default Admin..."
                password = make_password('TutorBoesK1')
                new_admin = {
                    "name": StringConstants.ADMIN_NAME,
                    "email": StringConstants.ADMIN_EMAIL,
                    "password": password,# Change after creation
                    "role": StringConstants.ADMIN_ROLE
                }
                serializer = AdminTutorSerializer(data=new_admin, many=False)
                if serializer.is_valid():
                    serializer.save()
                    print "Admin added"
                    
        if options['cp']:
            passwd = options['cp']
            valid = self.__validate_pass(passwd)
            if valid == '':
                try:
                    admin_tutor = AdminTutor.objects.get(email=StringConstants.ADMIN_EMAIL)
                    passwd = make_password(passwd)
                    new_admin = {
                        "name": StringConstants.ADMIN_NAME,
                        "email": StringConstants.ADMIN_EMAIL,
                        "password": passwd,
                        "role": StringConstants.ADMIN_ROLE
                    }
                    serializer = AdminTutorSerializer(admin_tutor, data=new_admin)
                    if serializer.is_valid():
                        serializer.save()
                        print "Admin password updated"
                    else: 
                        print serializer.errors
                except AdminTutor.DoesNotExist:
                    print "Default admin does not exist. Please use --ad option"